# README #

* Purpose and Quick summary
This test framework includes:

1) tools for conducting end-to-end tests (This comes with Playwright out of the box)
2) tools for conducting accessibility tests (The axe-core and axe-html-reporter have been integrated)

The framework is based on Playwright (<https://playwright.dev/>), which is a relative new test framework
that supports cross browser testing.

Supported browsers are:

Chrome, Edge, Firefox, and Safari.

## Future tools that could be included ##

1) REST API Testing (Playwright supports REST API Testing, but we need to try it out)
2) Visual regression testing (Playwright has the capability to take screendumps of the different states that
a web application can be in. This is a good start to create a visual regression testing framework. However, we need
time to explore this possibility). Visual regression testing can be used to make sure that new stylings or new components
does not break any unintended styling. When ever a developer has conducted a styling change, the visual regression testing will
inform the developer what visuals has been changed and thus, the developer can immediately see if she/he has made any unintended chyanges to
the visuals of the web application. Furthermore, the visual regression testing could play an important role when we substitute
our current Adapt frontend with a React.js frontend. Since the visual regression testing could then inform the developer, when a new React.js
component is looking correct compared to our existing Adapt courses.

* Version 0.0.1

## How do I get set up? ##

* How to setup and get started
1 Git clone this repository
2 Install latest Node.js
3 Run the command: npm install
4 Run the command: npx playwright install (This is only the first time you use the framework. This command installs all the relevant browser engines needed for cross browser testing)
5 Run the command: npx playwright test (Note that there a lot of different playwright commands that you can explore in the official documentation, but the most frequently used are also created here in the package.json file as scripts).
