import { Result } from 'axe-core';

class ReportBuilder {

    build(violations: Result[]): string {
        if (!violations) return '';


        return `
            <!DOCTYPE html>
            <html lang="en">
                ${this.getHeader()}
                    <body>
                    <div style="padding: 2rem">
                        <h3>
                            AXE Accessibility Results for ADAPT_ACCESSIBILITY_TEST project
                        </h3>
                        <div class="summarySection">
                        </div>
                        <h5>axe-core found <span class="badge badge-warning">8</span> violations</h5>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th style="width: 45%">Description</th>
                                    <th style="width: 15%">Axe rule ID</th>
                                    <th style="width: 23%">WCAG</th>
                                    <th style="width: 7%">Impact</th>
                                    <th style="width: 5%">Count</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row"><a href="#1" class="card-link">1</a></th>
                                    <td>Documents must have &lt;title&gt; element to aid in navigation</td>
                                    <td>document-title</td>
                                    <td>WCAG 2 Level A, WCAG 2.4.2</td>
                                    <td>serious</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#2" class="card-link">2</a></th>
                                    <td>&lt;html&gt; element must have a lang attribute</td>
                                    <td>html-has-lang</td>
                                    <td>WCAG 2 Level A, WCAG 3.1.1</td>
                                    <td>serious</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#3" class="card-link">3</a></th>
                                    <td>Document should have one main landmark</td>
                                    <td>landmark-one-main</td>
                                    <td>Best practice</td>
                                    <td>moderate</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#4" class="card-link">4</a></th>
                                    <td>Page should contain a level-one heading</td>
                                    <td>page-has-heading-one</td>
                                    <td>Best practice</td>
                                    <td>moderate</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <th scope="row"><a href="#5" class="card-link">5</a></th>
                                    <td>All page content should be contained by landmarks</td>
                                    <td>region</td>
                                    <td>Best practice</td>
                                    <td>moderate</td>
                                    <td>4</td>
                                </tr>
                            </tbody>
                        </table>

        `;
    }


    getHeader() {
        return `
        <head>
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <style>
            .violationCard {
                width: 100%;
                margin-bottom: 1rem;
            }
            .violationCardLine {
                display: flex;
                justify-content: space-between;
                align-items: start;
            }
            .learnMore {
                margin-bottom: 0.75rem;
                white-space: nowrap;
                color: #2557a7;
            }
            .card-link {
                color: #2557a7;
            }
            .violationNode {
                font-size: 0.75rem;
            }
            .wrapBreakWord {
                word-break: break-word;
            }
            .summary {
                font-size: 1rem;
            }
            .summarySection {
                margin: 0.5rem 0;
            }
            .hljs {
                white-space: pre-wrap;
                width: 100%;
                background: #f0f0f0;
            }
            p {
                margin-top: 0.3rem;
            }
            li {
                line-height: 1.618;
            }
        </style>
        <!-- Bootstrap CSS -->
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
            integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
            crossorigin="anonymous"
        />
        <script
            src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"
        ></script>
        <link
            rel="stylesheet"
            href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/stackoverflow-light.min.css"
        />
        <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
        <link
            rel="icon"
            href="https://www.deque.com/wp-content/uploads/2018/03/cropped-DQ_SecondaryLogo_HeroBlue_RGB-1-32x32.png"
            sizes="32x32"
        />
        <title>AXE Accessibility Results</title>
    </head>
        `;
    }
}

export default ReportBuilder;