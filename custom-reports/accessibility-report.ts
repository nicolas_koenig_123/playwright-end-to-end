import { Reporter } from '@playwright/test/reporter';
import { Result } from 'axe-core';
import { createHtmlReport } from 'axe-html-reporter';
import { promises as fs } from 'fs';
import * as path from 'path';

class AccessibilityReporter implements Reporter {

    async report(violations: Result[]): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const options = { 
                results: { violations: violations },
                options: {
                    projectKey: 'ADAPT_ACCESSIBILITY_TEST',
                    doNotCreateReportFile: true,
                }
            };
            const date: Date = new Date();
            const htmlReport: string = createHtmlReport(options);
            const fileName = `accessibilityReport_${date.getMilliseconds().toString()}.html`;
            const reportFilePath = path.join(__dirname, `../accessibility-reports/${fileName}`);
            fs.writeFile(reportFilePath, htmlReport)
            .then(() => {
                resolve(undefined);
            })
            .catch((error) => {
                reject('Costum reporter error');
            });
        });
    }
    
    onBegin(config, suite) {
        console.log(`* Begining the run with ${suite.allTests().length} tests`);
    }

    onTestBegin(test) {
        console.log(`* Starting test: ${test.title}`);
    }

    onTestEnd(test, result) {
        console.log(`* Finished test: ${test.title}: Test result: ${result.status}`);
    }

    onEnd(result) {
        console.log(`* Finished the run: ${result.status}`);
    }
}
export default AccessibilityReporter;