import { BrowserType } from '@playwright/test';
import { promises as fs } from 'fs';
import * as path from 'path';
import { pathToFileURL, URL } from 'url';


class ReportDisplayer {
    async display(firefox: BrowserType<{}>) {
        return new Promise<void>((resolve, reject) => {
            const reportsFilePath = path.join(__dirname, '../artifacts');
            fs.readdir(reportsFilePath)
            .then((files) => {
                files.forEach((file:string) => {
                   const filePath:string = path.join(reportsFilePath, file);
                   const fileUrl:URL = pathToFileURL(filePath);
                    
                    (async () => {
                        const browser = await firefox.launch();
                        const page = await browser.newPage();
                        await page.goto(fileUrl.toString());
                        await browser.close();
                    })();
                });
                resolve();
            })
            .catch((error) => {
                reject('Error in display report');
            });
        });
    }
}

export default ReportDisplayer;