import { test, expect } from '@playwright/test';
import { checkLockedStates } from '../util/stateCheck.spec';

test.describe('Life Locking', () => {

  test.beforeEach(async ({ page }) => {
    await page.goto('https://cdn-esim.contentservice.net/adapt-dev/adapt-platform/courses/life-menu/latest/index.html', { waitUntil: 'networkidle' });
  });

  test('Checking elements locked and unlocked states', async ({page}) => {
    test.setTimeout(60000);

    let lockCheck = new checkLockedStates();

    // Identify menu items and checking their locking state
    const locatorInstr = page.locator('div[role="button"]:has-text("Menu test instructions")');
    const locatorSeq = page.locator('div[role="button"]:has-text("Sequential Locked")');
    const locatorLast = page.locator('div[role="button"]:has-text("Last Locked")');
    const locatorFirst = page.locator('div[role="button"]:has-text("First Unlocked")');
    const locatorCust = page.locator('div[role="button"]:has-text("Custom Locked")');
    const locatorNot = page.locator('div[role="button"]:has-text("Not Locked")');
    const menuItems = [locatorInstr, locatorSeq, locatorLast, locatorFirst, locatorCust, locatorNot];
    await locatorInstr.waitFor({ state: 'visible'});
    await expect(locatorInstr).toBeVisible();
    lockCheck.menu('menu',menuItems,1,0,0,0,0,0);

    // click to open instructions page
    await locatorInstr.click();

    // Click the bottom text to get element in view for completion
    const instText = page.locator('[class*="block "]').last();
    await expect(instText).toBeVisible();
    await instText.click();
    await expect(instText).toHaveClass(/is-complete/);

    // Locate "next" button and check locked state
    const locatorNxtBtn = page.locator('text=Next').last();
    lockCheck.next(locatorNxtBtn,1);
    // click the next button
    await locatorNxtBtn.click();

    //--------------- Testing instructions page complete ---------------------

    
    // Complete the first question
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').first().click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,0);

    // Click the "home" button
    await page.locator('[aria-label="home"]').click();

    // Check locked state of menu items
    lockCheck.menu('menu',menuItems,1,1,0,0,0,0);

    // Open sub-menu 
    await locatorSeq.click();

    // Locate and check locked state of sub-menu items
    const locatorP1 = page.locator('div[role="button"]:has-text("Page 1")');
    const locatorP2 = page.locator('div[role="button"]:has-text("Page 2")');            
    const locatorP3 = page.locator('div[role="button"]:has-text("Page 3")'); 
    const subMenuItems = [locatorP1, locatorP2, locatorP3];
    await locatorP1.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,0,0);
    // Open first page
    await locatorP1.click();
    
    // Complete second question
    lockCheck.next(locatorNxtBtn,0); 
    await page.locator('label:has-text("Complete")').nth(1).click();
    await page.locator('text=Submit').nth(3).click();
    lockCheck.next(locatorNxtBtn,1); 
    // Proceed to next page
    await locatorNxtBtn.click();

    // Open course menu
    await page.locator('[aria-label="Open menu\\."]').click();

    //Loacting all pages in the course menu
    const locCourse11 = page.locator('button[data-option="page"]:has-text("Page 1")').nth(0);
    const locCourse12 = page.locator('button[data-option="page"]:has-text("Page 2")').nth(0);
    const locCourse13 = page.locator('button[data-option="page"]:has-text("Page 3")').nth(0);
    const locCourse21 = page.locator('button[data-option="page"]:has-text("Page 1")').nth(1);
    const locCourse22 = page.locator('button[data-option="page"]:has-text("Page 2")').nth(1);
    const locCourse23 = page.locator('button[data-option="page"]:has-text("Page 3")').nth(1);
    const locCourse31 = page.locator('button[data-option="page"]:has-text("Page 1")').nth(2);
    const locCourse32 = page.locator('button[data-option="page"]:has-text("Page 2")').nth(2);
    const locCourse33 = page.locator('button[data-option="page"]:has-text("Page 3")').nth(2);
    const locCourse41 = page.locator('button[data-option="page"]:has-text("Page 1")').nth(3);
    const locCourse42 = page.locator('button[data-option="page"]:has-text("Page 2")').nth(3);
    const locCourse43 = page.locator('button[data-option="page"]:has-text("Page 3")').nth(3);
    const locCourse51 = page.locator('button[data-option="page"]:has-text("Page 1")').nth(4);
    const locCourse52 = page.locator('button[data-option="page"]:has-text("Page 2")').nth(4);
    const locCourse53 = page.locator('button[data-option="page"]:has-text("Page 3")').nth(4);
    const courseMenuItems = [locCourse11, locCourse12, locCourse13, locCourse21, locCourse22, locCourse23, locCourse31, locCourse32, locCourse33, locCourse41, locCourse42, locCourse43, locCourse51, locCourse52, locCourse53];

    // Checking locked state of course menu pages
    lockCheck.menu('course',courseMenuItems,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0);

    // Open main menu and check locked state 
    const courseMainBtn = page.locator('button:has-text("Life Menu")');
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,0,0,0,0); 
    
    // Open sub-menu and check locked states
    await locatorSeq.click();
    await locatorP2.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,0); 
    await locatorP2.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,0); 
    await page.locator('label:has-text("Complete")').click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,1);
    //proceed to next page
    await locatorNxtBtn.click();

    //return to sub-menu and check locked state
    await page.locator('text=Up').click();
    await locatorP3.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,1);
    await locatorP3.click();

    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0);
    await page.keyboard.press('Escape');

    // Complete the page
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,1);


    //---------------- Sequential Locked pages complete -----------------------


    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,0,0,0);

    // Open sub-menu and check locked states
    await locatorLast.click();
    await locatorP1.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,0);
    await locatorP1.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,1);
    await page.locator('label:has-text("Complete")').click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,1);
    // Proceed to the next page
    await locatorNxtBtn.click();

    // Complete the first question
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').first().click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,0);

    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,0,0,0);

    // Open sub-menu and check locked states
    await locatorLast.click();
    await locatorP2.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,0);
    await locatorP2.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').nth(1).click();
    await page.locator('text=Submit').nth(3).click();
    lockCheck.next(locatorNxtBtn,1);

    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,0,0,0);

    // Open sub-menu and check locked states
    await locatorLast.click();
    await locatorP3.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,1);
    await locatorP3.click();

    // Complete the page
    //lockCheck.next(locatorNxtBtn,0); ---------------- Uncomment when ADAPT-370 is fixed
    await page.locator('label:has-text("Complete")').click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,1);


    //---------------- Last Locked pages complete -----------------------


    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,1,0,0);

    // Open sub-menu and check locked states
    await locatorFirst.click();
    await locatorP1.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,0,0);
    await locatorP1.click();

    // Complete the first question
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').first().click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,0);

    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,1,0,0);

    // Open sub-menu and check locked states
    await locatorFirst.click();
    await locatorP1.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,0,0);
    await locatorP1.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').nth(1).click();
    await page.locator('text=Submit').nth(3).click();
    lockCheck.next(locatorNxtBtn,1);

    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,1,0,0);

    // Open sub-menu and check locked states
    await locatorFirst.click();
    await locatorP2.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,1);
    await locatorP2.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,1);
    await page.locator('label:has-text("Complete")').click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,1);
    // Proceed to the next page
    await locatorNxtBtn.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').click();
    await page.locator('text=Submit').nth(1).click();
    //lockCheck.next(locatorNxtBtn,0); ---------------- Uncomment when ADAPT-1009 is fixed
    

    //------------------ First Unlocked pages complete -----------------------


    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0); //------- change when ADAPT-1009 is fixed
    //lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,1,1,0);

    // Open sub-menu and check locked states
    await locatorCust.click();
    await locatorP2.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,0); //---------- change when ADAPT-1009 is fixed
    //lockCheck.menu('menu',subMenuItems,0,1,0);
    await locatorP2.click();

    // Complete the first question
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').first().click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,0);

    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0); //------- change when ADAPT-1009 is fixed
    //lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,1,1,0);

    // Open sub-menu and check locked states
    await locatorCust.click();
    await locatorP2.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,0); //---------- change when ADAPT-1009 is fixed
    //lockCheck.menu('menu',subMenuItems,0,1,0);
    await locatorP2.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').nth(1).click();
    await page.locator('text=Submit').nth(3).click();
    lockCheck.next(locatorNxtBtn,1);

    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0);//------- change when ADAPT-1009 is fixed
    //lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,0,1,1,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,1,1,0);

    // Open sub-menu and check locked states
    await locatorCust.click();
    await locatorP3.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,0,1,1); 
    await locatorP3.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,1); //---------- change when ADAPT-370 is fixed
    //lockCheck.next(locatorNxtBtn,0);
    await page.locator('label:has-text("Complete")').click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,1);  //---------- change when ADAPT-370 is fixed
    //lockCheck.next(locatorNxtBtn,0);

    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0);//------- change when ADAPT-1009 is fixed
    //lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0);

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,1,1,0);

    // Open sub-menu and check locked states
    await locatorCust.click();
    await locatorP1.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,1);
    await locatorP1.click();

    // Complete the page
    lockCheck.next(locatorNxtBtn,1);
    await page.locator('label:has-text("Complete")').click();
    await page.locator('text=Submit').nth(1).click();
    lockCheck.next(locatorNxtBtn,1);
    

    //---------------- Custom Locked pages complete -----------------------


    //Open course menu and check locked states
    await page.locator('[aria-label="Open menu\\."]').click();
    lockCheck.menu('course',courseMenuItems,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1); 

    // Return to menu and check locked state
    await courseMainBtn.click();
    lockCheck.menu('menu',menuItems,1,1,1,1,1,1);

    // Open sub-menu and check locked states
    await locatorNot.click();
    await locatorP1.waitFor({ state: 'visible'});
    lockCheck.menu('menu',subMenuItems,1,1,1);
    await locatorP1.click();

  });
});