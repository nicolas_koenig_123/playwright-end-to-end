import { test, expect } from '@playwright/test';
import { checkCompletion } from '../../util/stateCheck.spec';

//To run - npx playwright test tests/componentTests/accordion.spec.ts

test.describe('Accordion', () => {

  test.beforeEach(async ({ page }) => {
    await page.goto('https://cdn-esim.contentservice.net/adapt-dev/adapt-platform/courses/help-and-how-to-test/latest/index.html#/', { waitUntil: 'networkidle' });
  
    await page.locator('div[role="button"]:has-text("Presentation")').click();
    await page.locator('div[role="button"]:has-text("Accordion")').click();

  });
  let completCheck = new checkCompletion();

  test('Items set to auto-collapse', async ({page}) => {
    
    // locating all items in the first accordion
    const item1 = page.locator('button[class*="accordion_item-btn"]').nth(0);
    const item2 = page.locator('button[class*="accordion_item-btn"]').nth(1);
    const item3 = page.locator('button[class*="accordion_item-btn"]').nth(2);
    const item4 = page.locator('button[class*="accordion_item-btn"]').nth(3);
    const item5 = page.locator('button[class*="accordion_item-btn"]').nth(4);
    const item6 = page.locator('button[class*="accordion_item-btn"]').nth(5);
    const items = [item1, item2, item3, item4, item5, item6];

    checkCollapsed(items,0,0,0,0,0,0);

  });

  test('Items dont auto-collapse', async ({page}) => {
    
    // locating all items in the second accordion
    const item1 = page.locator('button[class*="accordion_item-btn"]').nth(6);
    const item2 = page.locator('button[class*="accordion_item-btn"]').nth(7);
    const item3 = page.locator('button[class*="accordion_item-btn"]').nth(8);
    const item4 = page.locator('button[class*="accordion_item-btn"]').nth(9);
    const item5 = page.locator('button[class*="accordion_item-btn"]').nth(10);
    const item6 = page.locator('button[class*="accordion_item-btn"]').nth(11);
    const items = [item1, item2, item3, item4, item5, item6];

    checkCollapsed(items,0,0,0,0,0,0);

  });

  function checkCollapsed (itemList, ...states) {

    // loop through items and check collapsed state
    
        expect(itemList[0]).toBeVisible();

      for (let i = 0; i < states.length; i++){
        if (states[i] == 1){
        expect(itemList[i]).toHaveAttribute('aria-expanded', "true");
        }
        else if (states[i] == 0){
        expect(itemList[i]).toHaveAttribute('aria-expanded', "false");
        }
      }
  }

});