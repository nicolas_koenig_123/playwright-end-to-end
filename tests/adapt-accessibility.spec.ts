import { test, expect, firefox } from '@playwright/test';
import { injectAxe, checkA11y, getViolations, reportViolations, Options } from 'axe-playwright'
import AccessibilityReporter from '../custom-reports/accessibility-report';
import ReportDisplayer from '../custom-reports/report-displayer';

test.describe('Adapt course accessibility test', () => {
  
  test.beforeEach(async ({ page }) => {
    await page.goto('https://cdn-esim.contentservice.net/adapt-dev/adapt-platform/courses/help-and-how-to-test/latest/');
    await injectAxe(page);
  });
  
  test('startpage should be accessibile', async ({ page }) => {
    try {
      const violations = await getViolations(page, {
        axeOptions: {
          runOnly: {
            type: 'tag',
            values: ['wcag2a'],
          },
        },
      });
      await new AccessibilityReporter().report(violations);      
      expect(violations.length).toBe(0);
      
    }
    catch(error) {
      console.log('accessibility reporter error: ', error);
    }
  });
});

