
import { test, expect } from '@playwright/test';

test.describe('Adapt Help and how to', () => {

  test.beforeEach(async ({ page }) => {
      await page.goto('https://cdn-esim.contentservice.net/adapt-dev/adapt-platform/courses/help-and-how-to-test/latest/index.html', { waitUntil: 'networkidle' });
  });

  test('Go to General Testing Instructions page', async ({ page }) => {
    try {
      await page.click('text="General Testing Instructions"');
      const subtitle = page.locator('text="Some things to help test the course"');
      await expect(subtitle).toBeVisible();
    }
    catch(error) {
      console.log('Go to page General Testing Instruction error: ', error);
    }
  });

  test('Go to presentation components page', async ({ page }) => {
    try {
      await page.click('text=Presentation');
      const accordion = page.locator('text=Accordion');
      await expect(accordion).toBeVisible();
    }
    catch(error) {
      console.log('Go to page General Testing Instruction error: ', error);
    }
  });

  test('Open accordion item with graphic ', async ({ page }) => {   
    await page.goto('https://cdn-esim.contentservice.net/adapt-dev/adapt-platform/courses/help-and-how-to-test/latest/index.html#/id/609e21e72eb9f77dd468a85e',  { waitUntil: 'networkidle' });    
    await page.click('text="Item with a graphic"');
    const accordionContent = page.locator('img:visible');
    await accordionContent.waitFor({ state: 'visible'});
    await expect(accordionContent).toBeVisible();
  });

});

