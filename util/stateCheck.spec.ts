import { expect } from '@playwright/test';

export class checkLockedStates {

    menu(type, itemList, ...states) {

        // loop through items and check locked state 
        if(type == 'menu'){
            expect(itemList[0]).toBeVisible();

          for (let i = 0; i < states.length; i++){
            if (states[i] == 1){
            expect(itemList[i]).not.toHaveClass(/is-locked/);
            }
            else if (states[i] == 0){
            expect(itemList[i]).toHaveClass(/is-locked/);
            }
          }
        }else if(type == 'course'){
          for (let i = 0; i < states.length; i++){
            if (states[i] == 1){
            expect(itemList[i]).not.toHaveClass(/disabled/);
            }
            else if (states[i] == 0){
            expect(itemList[i]).toHaveClass(/disabled/);
            }
          }
        }
      }

    next(nxtBtn, state){

        expect(nxtBtn).toBeVisible();

        if (state == 1){
            expect(nxtBtn).not.toHaveClass(/is-disabled/);
        }
        else if (state == 0){
            expect(nxtBtn).toHaveClass(/is-disabled/);
        }
    }

}

export class checkCompletion {

  block(locator, state){

    if (state == 1){
      expect(locator).toHaveClass(/is-complete/);
    }
    else if (state == 0){
      expect(locator).not.toHaveClass(/is-complete/);
    }
  }

}